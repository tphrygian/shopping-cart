<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function shop()
    {
        $products = Product::all();        

        return view('shop')
                ->withTitle('E-COMMERCE STORE | SHOP')
                ->with(['products' => $products]);
    }

    public function cart() 
    {
        $cartCollection = \Cart::getContent();        

        return view('cart')
                ->withTitle('E-COMMERCE STORE | CART')
                ->with(['cartCollection' => $cartCollection]);
    }
}
